from django.views.generic.list import ListView
from todos.models import TodoList
from django.views.generic.detail import DetailView


class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"
